//
//  Preferences.swift
//  NoMAD Pro
//
//  Created by Joel Rennich on 4/24/17.
//  Copyright © 2017 Orchard & Grove. All rights reserved.
//

import Foundation

/// A convenience name for `UserDefaults.standard`
let defaults = UserDefaults.init()

enum Preferences: String, CaseIterable {
    
    typealias RawValue = String
    
    case
    allowTokend,
    allowSoftCerts,
    cnFilterString,
    defaultKerberosDomain,
    identities,
    showPIVAuthentication,
    showPIVSignature,
    showKeyEncipherment,
    showAll
}

func printAllPrefs() {
        for key in Preferences.allCases {
            
            let pref = defaults.object(forKey: key.rawValue) as AnyObject
            
            switch String(describing: type(of: pref)) {
            case "__NSCFBoolean" :
                print("\t" + key.rawValue + ": " + String(describing: ( defaults.bool(forKey: key.rawValue))))
            case "__NSCFArray" :
                print("\t" + key.rawValue + ": " + ( String(describing: (defaults.array(forKey: key.rawValue)!))))
            case "__NSTaggedDate", "__NSDate" :
                print("\t" + key.rawValue + ": " + ( defaults.object(forKey: key.rawValue) as! Date ).description(with: Locale.current))
            case "__NSCFDictionary":
                print("\t" + key.rawValue + ": " + String(describing: defaults.dictionary(forKey: key.rawValue)!))
            case "__NSCFData" :
                print("\t" + key.rawValue + ": " + (defaults.data(forKey: key.rawValue)?.base64EncodedString() ?? "ERROR"))
            case "__NSCFNumber" :
                print("\t" + key.rawValue + ": " + String(describing: defaults.integer(forKey: key.rawValue)))
            default :
                print("\t" + key.rawValue + ": " + ( defaults.object(forKey: key.rawValue) as? String ?? "Unset"))
            }
            
            if defaults.objectIsForced(forKey: key.rawValue) {
                print("\t\tForced")
            }
            print("\n")
        }
}
