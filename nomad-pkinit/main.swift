//
//  main.swift
//  nomad-pkinit
//
//  Created by Joel Rennich on 8/22/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Foundation
import SystemConfiguration

let keychain = KeychainUtil()

let kutil = KUtil()

var PIN = ""
var user: String? = nil
var secRef: SecIdentity? = nil
var needPIN: Int? = nil
var token = false
var cardKeychain: SecKeychain? = nil

// looping

var keepAlive = false

var secID: Unmanaged<SecIdentity>? = nil

let myWorkQueue = DispatchQueue(label: "menu.nomad.nomad-pkinit.background_work_queue", attributes: [])

private let kCryptoExportImportManagerPublicKeyInitialTag = "-----BEGIN RSA PUBLIC KEY-----\n"
private let kCryptoExportImportManagerPublicKeyFinalTag = "-----END RSA PUBLIC KEY-----\n"

private let kCryptoExportImportManagerRequestInitialTag = "-----BEGIN CERTIFICATE REQUEST-----\n"
private let kCryptoExportImportManagerRequestFinalTag = "-----END CERTIFICATE REQUEST-----\n"

private let kCryptoExportImportManagerPublicNumberOfCharactersInALine = 64

func PEMKeyFromDERKey(_ data: Data, PEMType: String) -> String {
    
    var resultString: String
    
    // base64 encode the result
    let base64EncodedString = data.base64EncodedString(options: [])
    
    // split in lines of 64 characters.
    var currentLine = ""
    if PEMType == "RSA" {
        resultString = kCryptoExportImportManagerPublicKeyInitialTag
    } else {
        resultString = kCryptoExportImportManagerRequestInitialTag
    }
    var charCount = 0
    for character in base64EncodedString {
        charCount += 1
        currentLine.append(character)
        if charCount == kCryptoExportImportManagerPublicNumberOfCharactersInALine {
            resultString += currentLine + "\n"
            charCount = 0
            currentLine = ""
        }
    }
    // final line (if any)
    if currentLine.count > 0 { resultString += currentLine + "\n" }
    // final tag
    if PEMType == "RSA" {
        resultString += kCryptoExportImportManagerPublicKeyFinalTag
    } else {
        resultString += kCryptoExportImportManagerRequestFinalTag
    }
    return resultString
}

// check for looping

if CommandLine.arguments.contains("-network") && CommandLine.arguments.contains("-silent"){
    // register for SC callbacks
    keepAlive = true
}

func run() {
    
    // app to perform pkinit from the CLI
    
    print("-- nomad-pkinit 1.0.1 build 16 --")
    
    if CommandLine.arguments.contains("-h") || CommandLine.arguments.contains("-help") {
        print("nomad-pkinit - a CLI method for getting tickets via PKINIT.")
        print("By default nomad-pkinit will look at the menu.nomad.NoMAD preference domain")
        print("for any existing account mappings between pubkey hashes and Kerberos principals.")
        print("if found, nomad-pkinit will let the user pick one of those first.")
        print("Otherwise, nomad-pkinit will show all possible identities that can be found and")
        print("allow the user to pick one. After which it will prompt for a PIN.")
        print("")
        print("nomad-pkinit can use CTK, tokend or soft certs based upon preferences in the")
        print("menu.nomad.NoMAD preference file.")
        print("To show all identities regardless of type, use the \"-all\" flag.")
        print("Also, to get the OIDs of an identity, use the \"-oid\" flag and then select an")
        print("identity through the menus. No PKINIT will be attempted, but the identity's OIDs")
        print("will be shown.")
        print("The \"-export\" flag will export the selected identity in PEM format.")
        print("")
        print("To show the issuing CA of a certificate, use the \"-ca\" flag.")
        print("Using \"-caOnly\" followed by a CA name will filter all identities to only those matching that CA")
        print("Using \"-network\" will cause nomad-pkinit to wait for network changes, and then")
        print("automatically authenticate if possible.")
        exit(0)
    }
    
    // check for network notifications
    
    // check for -caOnly
    
    if CommandLine.arguments.contains("-caOnly") {
        // get the CA
        
        for i in 0...(CommandLine.arguments.count - 1) {
            if CommandLine.arguments[i] == "-caOnly" {
                
                if CommandLine.arguments.count < i + 1 {
                    print("Invalid filtering request")
                    exit(0)
                }
                
                print("Filtering on CA: \(CommandLine.arguments[i + 1])")
                keychain.caMatch = CommandLine.arguments[i + 1]
            }
        }
    }
    
    // check for -caOnly
    
    if CommandLine.arguments.contains("-caOrgOnly") {
        // get the CA Organization
        
        for i in 0...(CommandLine.arguments.count - 1) {
            if CommandLine.arguments[i] == "-caOrgOnly" {
                
                if CommandLine.arguments.count < i + 1 {
                    print("Invalid filtering request")
                    exit(0)
                }
                
                print("Filtering on CA Organization: \(CommandLine.arguments[i + 1])")
                keychain.caOrgMatch = CommandLine.arguments[i + 1]
            }
        }
    }
    
    // check for filtering
    
    if CommandLine.arguments.contains("-filter") {
        // get the CA
        
        for i in 0...(CommandLine.arguments.count - 1) {
            if CommandLine.arguments[i] == "-filter" {
                
                if CommandLine.arguments.count < i + 2 {
                    print("Invalid filtering request")
                    exit(0)
                }
                
                print("Filtering on OID: \(CommandLine.arguments[i + 1] )")
                print("Filtering for value: \(CommandLine.arguments[i + 2] )")
                keychain.filterOID = CommandLine.arguments[i + 1]
                keychain.filterTerm = CommandLine.arguments[i + 2]
            }
        }
    }
    
    let identities = keychain.findIdentities(tokensOnly: 0)
    
    if identities.count < 1 {
        print("No identies to use for PKINIT")
        
        exit(0)
    }
    
    // find any NoMAD mappings
    
    let presetIdentities = defaults.dictionary(forKey: Preferences.identities.rawValue) ?? [String:Any]()
    
    if (presetIdentities.count) == 0 && !CommandLine.arguments.contains("-oids") && !CommandLine.arguments.contains("-export") {
        for id in presetIdentities {
            if (id.value as! String) != "none" {
                for identity in identities {
                    if (id.value as! String) == identity.pubKeyHash {
                        print(" Use \(id.key)? (y/n)", terminator: ": ")
                        let response = readLine(strippingNewline: true)
                        if response == "y" || response == "" {
                            user = id.key
                            secRef = identity.identity
                            needPIN = identity.keychain
                        }
                    }
                }
            }
        }
    }
    
    // if silent... try to get ALL the tickets
    
    if CommandLine.arguments.contains("-silent") {
        let kutil = KUtil()
        
        //checkBeta()
        
        for identity in identities {
            if identity.keychain == 0 && identity.principal != nil && identity.principal != "none" {
                print("     Getting Ticket for \(identity.principal!)")
                
                let error = kutil.getCreds(cert: identity.identity, user: identity.principal!)

                if error != "" {
                    print("     Error: " + error)
                } else {
                    print("     Ticket aquired for \(identity.principal!).")
                }
            }
        }
        
        if !keepAlive {
            exit(0)
        } else {
            return
        }
    }
    
    if user == nil {
        var loop = 1
        print("")
        print(" Pick an identity to use:")
        for identity in identities {
            if CommandLine.arguments.contains("-ca") {
                print(" \(loop): \(identity.cn) - \(identity.ca ?? "no CA")")
            } else if CommandLine.arguments.contains("-caOrg") {
                print(" \(loop): \(identity.cn) - \(identity.caOrg ?? "no CA Organization")")
            } else {
                print(" \(loop): \(identity.cn ?? "unkown identity")")
            }
            loop += 1
        }
        
        var finshed = false
        var pickedID = ""
        
        while !finshed {
            print("   Enter identity to use", terminator: ": ")
            
            pickedID = readLine(strippingNewline: true)!
            
            // check if there's only one item and the response is blank
            
            if pickedID == "" && loop == 2 {
                pickedID = "1"
            }
            
            // check for q or quit
            
            if pickedID == "q" || pickedID == "quit" {
                print("    Quitting")
                exit(0)
            }
            
            // check for text
            
            if (Int(pickedID) == nil) {
                print("    Invalid selection")
                continue
            }
            
            if Int(pickedID)! > ( loop - 1 ) || (Int(pickedID) == 0) {
                print("    Invalid selection")
                continue
            }
            finshed = true
        }
        
        let pickedNum = (Int(pickedID)! - 1)
        
        secRef = identities[pickedNum].identity
        needPIN = identities[pickedNum].keychain
        cardKeychain = identities[pickedNum].keychainRef
        
        if CommandLine.arguments.contains("-oids") {
            print(identities[pickedNum].oids)
            exit(0)
        }
        
        if CommandLine.arguments.contains("-export") {
            let id = identities[pickedNum].identity
            var publicCert: SecCertificate? = nil
            
            let err = SecIdentityCopyCertificate(id, &publicCert)
            
            if err == 0 {
                let certData = SecCertificateCopyData(publicCert!)
                print(PEMKeyFromDERKey(certData as Data, PEMType: "RSA"))
            } else {
                print("Error getting public certificate.")
            }
            exit(0)
        }
        
        
        // get user principal
        
        if identities[pickedNum].principal != nil {
            
            let name = identities[pickedNum].principal!
            print("   Enter user principal, or blank for \(name)", terminator: ": ")
            
            user = readLine(strippingNewline: true)!
            if user == "" {
                user = identities[pickedNum].principal
            }
        } else {
            print("   Enter user principal", terminator: ": ")
            user = readLine(strippingNewline: true)!
        }
        
        // now clean the principal
        
        // check to ensure it's a full principal, if not try to complete it
        
        if !(user?.contains("@"))! {
            user = user! + "@" + (defaults.string(forKey: Preferences.defaultKerberosDomain.rawValue) ?? "")
        }
        
        // to be extra nice, ensure that the REALM is in uppercase
        
        let userParts = user?.components(separatedBy: "@")
        
        user = (userParts?.first)! + "@" + (userParts?.last?.uppercased())!
    }
    
    // see if we need a pin
    
    if (needPIN != 0) {
        
        // identity is not a soft cert, so get a PIN
        
        let p = getpass(" Enter PIN: ")
        
        PIN = String(cString: p!)
        
        if needPIN ==  1 {
            token = true
        } else {
            // make sure the card is unlocked
            
            var myErr: OSStatus?
            myErr = SecKeychainUnlock(cardKeychain, UInt32(PIN.lengthOfBytes(using: String.defaultCStringEncoding)), PIN, true)
            if myErr != 0 {
                // we couldn't unlock the keychain
                print("   Error unlocking the keychain.")
                exit(0)
            }
        }
    }
        
    // see if we can use the native-Swift method
    
    if ( needPIN == 0) {
        
        print("Getting Ticket...")
        
        let error = kutil.getCreds(cert: secRef!, user: user!)
        
        if error != "" {
            print(error)
        } else {
            print("Ticket aquired.")
        }
    } else {
        
        // bit of sleight of hand to allow for ObjC things
        
        let id2 = Unmanaged<SecIdentity>.passRetained(secRef!)
        secID = id2
        
        let kerbError = kutil.getCreds(cert: secRef!, user: user!, pin: PIN)
        
        while kutil.running {
            RunLoop.current.run(mode: RunLoop.Mode.default, before: Date.distantFuture)
        }
        
        if kerbError != "" {
            // error getting credentials
            print("   Error: " + kerbError)
        }
    }
    //checkBeta()
}

// make it happen

let changed : SCDynamicStoreCallBack = { dynamicStore, _, _ in
    // update the kerb tickets
    print("triggering")
    try? "running".write(toFile: "/tmp/nomad-pkinit", atomically: true, encoding: String.Encoding.utf8)
    run()
}

if keepAlive {
    print("registering for SC Callbacks")
    
    RunLoop.main.perform {
        
        var dynamicContext = SCDynamicStoreContext(version: 0, info: nil, retain: nil, release: nil, copyDescription: nil)
        
        let dcAddress = withUnsafeMutablePointer(to: &dynamicContext, {UnsafeMutablePointer<SCDynamicStoreContext>($0)})
        
        if let dynamicStore = SCDynamicStoreCreate(kCFAllocatorDefault, "menu.nomad.NoMAD.networknotification" as CFString, changed, dcAddress) {
            let keysArray = ["State:/Network/Global/IPv4" as CFString, "State:/Network/Global/IPv6"] as CFArray
            SCDynamicStoreSetNotificationKeys(dynamicStore, nil, keysArray)
            let loop = SCDynamicStoreCreateRunLoopSource(kCFAllocatorDefault, dynamicStore, 0)
            CFRunLoopAddSource(CFRunLoopGetCurrent(), loop, .defaultMode)
            print("registered")
        }
    }
}

run()

while keepAlive {
    RunLoop.current.run(mode: RunLoop.Mode.default, before: Date.distantFuture)
}

